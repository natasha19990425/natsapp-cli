import {
  Controller,
  Replier,
  Subscriber,
  JetStreamServiceProvider,
} from '@his-base/jetstream';
import { Codec, JsMsg, Msg } from 'nats';

@Controller('{name}')
export class {CamelName}Controller {
  jetStreamService = JetStreamServiceProvider.get();

  @Subscriber('insert')
  insert{CamelName}(message: JsMsg, payload: any) {
    try {
      // Handle Insert {CamelName}

      message.ack();
    } catch (error) {
      console.log('Error processing {name}.insert: ', error);
      message.nak();
    }
  }

  @Replier('list')
  get{CamelName}s(message: Msg, payload: any, jsonCodec: Codec<any>) {
    const {camelName}s = [];

    message.respond(jsonCodec.encode({camelName}s));
  }
}
