import fs from 'node:fs';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import { formatName, getFileGenerator } from './utils.js';
const __dirname = path.dirname(fileURLToPath(import.meta.url));
const userDirname = process.cwd();
export function generateProject(name) {
    const { kebabCase } = formatName(name);
    const templatesPath = path.join(__dirname, '../templates');
    const projectPath = path.join(userDirname, kebabCase);
    fs.mkdirSync(projectPath, { recursive: true });
    const generateFile = getFileGenerator(projectPath, templatesPath);
    // Generate .env.template
    generateFile('.env.template.template');
    // Generate .gitignore
    generateFile('.gitignore.template');
    // Generate package.json
    generateFile('package.template.json', {
        replacements: [{ searchValue: /\{name\}/g, replaceValue: kebabCase }],
    });
    // Generate tsconfig.json
    generateFile('tsconfig.template.json');
    // Generate files in src/
    generateFile('src/main.template.ts');
    generateFile('src/server.config.template.ts');
    // Generate controllers/index.ts
    generateFile('src/controllers/index.template.ts');
    // Generate controller
    generateController('app', projectPath);
}
export function generateController(name, projectPath) {
    const { lowerCamelCase, upperCamelCase, kebabCase } = formatName(name);
    projectPath !== null && projectPath !== void 0 ? projectPath : (projectPath = userDirname);
    const controllersPath = path.join(projectPath, 'src/controllers');
    if (!fs.existsSync(controllersPath)) {
        if (fs.existsSync(path.join(projectPath, 'package.json')))
            fs.mkdirSync(controllersPath, { recursive: true });
        else
            console.error('Not in a project!');
    }
    const generateFile = getFileGenerator(controllersPath, path.join(__dirname, '../templates/src/controllers'));
    generateFile('controller.template.ts', {
        replacements: [
            { searchValue: /\{name\}/g, replaceValue: kebabCase },
            { searchValue: /\{camelName\}/g, replaceValue: lowerCamelCase },
            { searchValue: /\{CamelName\}/g, replaceValue: upperCamelCase },
        ],
        filename: `${kebabCase}.controller.ts`,
    });
    fs.appendFileSync(path.join(controllersPath, 'index.ts'), `export * from \'./${kebabCase}.controller\';\n`);
}
