#!/usr/bin/env node
import { program } from 'commander';
import { generateController, generateProject } from './generate.js';
program.version('1.0.0').description('Project Code Generator');
program
    .command('new <name>')
    .description('Generate a complete project')
    .action((name) => {
    generateProject(name);
});
program
    .command('c <name>')
    .description('Generate a controller')
    .action((name) => {
    generateController(name);
});
program.parse(process.argv);
