import fs from 'node:fs';
import path from 'node:path';
export function getFileGenerator(generatePath, templatesPath) {
    return function generateFile(templateFilePath, config = {
        replacements: [],
    }) {
        const relativeDirPath = path.dirname(templateFilePath);
        const generateDirPath = path.join(generatePath, relativeDirPath);
        //判斷是否需創建project資料夾
        if (!fs.existsSync(generateDirPath)) {
            fs.mkdirSync(generateDirPath, { recursive: true });
        }
        const templatePath = path.join(templatesPath, templateFilePath);
        const templateContent = readFile(templatePath);
        let content = templateContent;
        for (const r of config.replacements) {
            content = content.replace(r.searchValue, r.replaceValue);
        }
        const outputFilePath = path.join(generatePath, config.filename || templateFilePath.replace('.template', ''));
        writeFile(outputFilePath, content);
    };
}
function readFile(filePath) {
    return fs.readFileSync(filePath, 'utf-8');
}
function writeFile(filePath, content) {
    fs.writeFileSync(filePath, content);
}
export function capitalize(s) {
    return s.charAt(0).toUpperCase() + s.slice(1);
}
export function formatName(name) {
    function toKebab(str) {
        return str.replace(/[A-Z]+(?![a-z])|[A-Z]/g, function (matched, next) {
            return (next ? '-' : '') + matched.toLowerCase();
        });
    }
    const kebabCase = name.includes('-') ? name : toKebab(name);
    function toLowerCamel(str) {
        return str.replace(/-+([a-z])/g, function (_, next) {
            return next.toUpperCase();
        });
    }
    const lowerCamelCase = toLowerCamel(kebabCase);
    const upperCamelCase = capitalize(lowerCamelCase);
    return {
        kebabCase,
        lowerCamelCase,
        upperCamelCase,
    };
}
