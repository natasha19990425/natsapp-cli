import fs from 'node:fs';
import path from 'node:path';

export function getFileGenerator(generatePath: string, templatesPath: string) {
    return function generateFile(
        templateFilePath: string,
        config: { replacements: { searchValue: RegExp; replaceValue: string }[]; filename?: string } = {
            replacements: [],
        }
    ) {
        const relativeDirPath = path.dirname(templateFilePath);
        const generateDirPath = path.join(generatePath, relativeDirPath);

        // 判斷是否需創建project資料夾
        if (!fs.existsSync(generateDirPath)) {
            fs.mkdirSync(generateDirPath, { recursive: true });
        }

        const templatePath = path.join(templatesPath, templateFilePath);
        const templateContent = readFile(templatePath);

        let content = templateContent;
        for (const r of config.replacements) {
            content = content.replace(r.searchValue, r.replaceValue);
        }

        const outputFilePath = path.join(
            generatePath,
            config.filename || templateFilePath.replace('.template', '')
        );
        writeFile(outputFilePath, content);
    };
}

function readFile(filePath: string): string {
    return fs.readFileSync(filePath, 'utf-8');
}

function writeFile(filePath: string, content: string) {
    fs.writeFileSync(filePath, content);
}

export function capitalize(s: string): string {
    return s.charAt(0).toUpperCase() + s.slice(1);
}

// CLI輸入大寫會自動加入-進行分割
export function formatName(name: string) {
  function toKebab(str: string) {
    return str.replace(/[A-Z]+(?![a-z])|[A-Z]/g, function (matched, next) {
      return (next ? '-' : '') + matched.toLowerCase();
    });
  }

  const kebabCase = name.includes('-') ? name : toKebab(name);

  function toLowerCamel(str: string) {
    return str.replace(/-+([a-z])/g, function (_, next) {
      return next.toUpperCase();
    });
  }

  const lowerCamelCase = toLowerCamel(kebabCase);
  const upperCamelCase = capitalize(lowerCamelCase);

  return {
    kebabCase,
    lowerCamelCase,
    upperCamelCase,
  };
}